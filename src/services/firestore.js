import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCGy33aYKqXLge-pa_5OWAIoAErwMg1Odc",
    authDomain: "creatorya-8e159.firebaseapp.com",
    databaseURL: "https://creatorya-8e159.firebaseio.com",
    projectId: "creatorya-8e159",
    storageBucket: "creatorya-8e159.appspot.com",
    messagingSenderId: "760636520325",
    appId: "1:760636520325:web:d5ebfb5a1fa197a454f6ec",
    measurementId: "G-W1XBBBKHTX"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export const getProductPostItItems = productId => {
    return db.collection('products')
        .doc(productId)
        .collection('post-it-items')
        .get();
}


export const streamProductPostItItems = (productId,observer) => {
    return db.collection('products')
        .doc(productId)
        .collection('post-it-items')
        .orderBy('created')
        .onSnapshot(observer);
};


export const addPostItItem = (productId,postItItem) => {
    return db.collection('products')
        .doc(productId)
        .collection('post-it-items')
        .doc(postItItem.id)
        .set({
            ...postItItem,
            created: firebase.firestore.FieldValue.serverTimestamp(),
        });
};