import React,{ useState } from 'react';

function TopMenu({ user,changeUser,project,setProject }) {
    const [isMenuOpen,setIsMenuOpen] = useState(false);
    const mobileMenuItemClass = itemProject => itemProject === project
        ? "block pl-3 pr-4 py-2 border-l-4 border-indigo-500 text-base font-medium text-indigo-700 bg-indigo-50 focus:outline-none focus:text-indigo-800 focus:bg-indigo-100 focus:border-indigo-700 transition duration-150 ease-in-out"
        : "block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out";
    const menuItemClass = itemProject => itemProject === project
        ? "inline-flex items-center px-1 pt-1 border-b-2 border-indigo-500 text-sm font-medium leading-5 text-gray-900 focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out"
        : "inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out";
    return (
        <nav class="bg-white border-b border-gray-200">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="flex justify-between h-16">
                    <div class="flex">
                        <div class="flex-shrink-0 flex items-center">
                            <h1 class="text-3xl font-bold leading-tight text-gray-900">
                                Creatorya
                                </h1>
                        </div>
                        <div class="hidden sm:-my-px sm:ml-6 sm:flex">
                            {/* Missing stage of choosing what you work on so challange selector */}
                            <a onClick={() => setProject("too-easy-business")} class={"ml-8 " + menuItemClass("too-easy-business")}>
                                Too Easy Business
                            </a>
                            <a onClick={() => setProject("too-easy-catering")} class={"ml-8 " + menuItemClass("too-easy-catering")}>
                                Too Easy Catering
                            </a>
                            <a onClick={() => setProject("paths-of-history")} class={"ml-8 " + menuItemClass("paths-of-history")}>
                                Paths of Hi.story
                            </a>
                        </div>
                    </div>


                    <div class="hidden sm:ml-6 sm:flex sm:items-center">
                        <button onClick={changeUser} class="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-gray-500 focus:outline-none focus:text-gray-500 focus:bg-gray-100 transition duration-150 ease-in-out" aria-label="Notifications">
                            {user}
                        </button>
                    </div>

                    <div class="-mr-2 flex items-center sm:hidden">
                        {/* <!-- Mobile menu button --> */}
                        <button onClick={() => setIsMenuOpen(!isMenuOpen)} class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                            {/* <!-- Menu open: "hidden", Menu closed: "block" --> */}
                            <svg class="block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                            </svg>
                            {/* <!-- Menu open: "block", Menu closed: "hidden" --> */}
                            <svg class="hidden h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>

            {/* <!--
      Mobile menu, toggle classes based on menu state.

      Open: "block", closed: "hidden"
    --> */}
            <div class={isMenuOpen ? 'block' : 'hidden sm:hidden'}>
                <div class="pt-2 pb-3">
                    <a onClick={() => { setIsMenuOpen(false); setProject("too-easy-business"); }} class={mobileMenuItemClass("too-easy-business")}>
                        Too Easy Business
                            </a>
                    <a onClick={() => { setIsMenuOpen(false); setProject("too-easy-catering"); }} class={mobileMenuItemClass("too-easy-catering")}>
                        Too Easy Catering
                            </a>
                    <a onClick={() => { setIsMenuOpen(false); setProject("paths-of-history"); }} class={mobileMenuItemClass("paths-of-history")}>
                        Paths of Hi.story
                            </a>
                </div>
                <div class="pt-4 pb-3 border-t border-gray-200">
                    <div class="flex items-center px-4">
                        <button onClick={changeUser} class="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-gray-500 focus:outline-none focus:text-gray-500 focus:bg-gray-100 transition duration-150 ease-in-out" aria-label="Notifications">
                            {user}
                        </button>
                    </div>
                </div>
            </div>
        </nav>


    );
}

export default TopMenu;