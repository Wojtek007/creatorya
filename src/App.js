import React,{ useState,useEffect } from 'react';
import './App.css';
import './tailwind.output.css';
import TopMenu from './top-menu/top-menu';
import ScopingCanvas from './understand-challenge/scoping-canvas';

function App() {
  const [user,setUser] = useState(localStorage.getItem('creatorya-user') || 'wojtek');
  const [project,setProject] = useState(localStorage.getItem('creatorya-project') || "the-camp");
  const changeUser = () => setUser(user === "wojtek" ? "valeriya" : "wojtek");

  useEffect(() => {
    localStorage.setItem('creatorya-user',user);
  },[user]);
  useEffect(() => {
    localStorage.setItem('creatorya-project',project);
  },[project]);

  return (
    <div class="min-h-screen bg-white">
      <TopMenu user={user} changeUser={changeUser} project={project} setProject={setProject} />
      <ScopingCanvas user={user} project={project} />
    </div>
  );
}

export default App;
