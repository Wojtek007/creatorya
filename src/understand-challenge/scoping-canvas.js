import React,{ useState,useEffect } from 'react';
import PostItFrame from './post-it-frame';
import * as FirestoreService from '../services/firestore';

const weekFinish = '08/14/2020 10:1 PM';

function ScopingCanvas({ user,project }) {

  const [postItItems,setPostItItems] = useState([]);
  const [error,setError] = useState('');
  const [timeLeft,setTimeLeft] = useState('');

  useEffect(() => {
    const unsubscribe = FirestoreService.streamProductPostItItems(project,{
      next: querySnapshot => {
        const updatedPostItItems =
          querySnapshot.docs.map(docSnapshot => docSnapshot.data());
        setPostItItems(updatedPostItItems);
      },
      error: () => setError('subscribe-fail')
    });
    return unsubscribe;
  },[project,setPostItItems]);

  useEffect(() => {
    const _second = 1000;
    const _minute = _second * 60;
    const _hour = _minute * 60;
    const _day = _hour * 24;
    let timer;

    const end = new Date(weekFinish);

    function showRemaining() {
      const now = new Date();
      const distance = end - now;
      if(distance < 0) {
        clearInterval(timer);

        setTimeLeft('EXPIRED!');
        return
      }
      const days = Math.floor(distance / _day);
      const hours = Math.floor((distance % _day) / _hour);
      const minutes = Math.floor((distance % _hour) / _minute);
      const seconds = Math.floor((distance % _minute) / _second);

      let result = days + 'days ';
      result += hours + 'hrs ';
      result += minutes + 'mins ';
      result += seconds + 'secs';
      setTimeLeft(result);
    }

    timer = setInterval(showRemaining,1000);


    return () => { clearInterval(timer); }
  })


  if(error !== '') {
    return <div>{error}</div>
  }

  return (
    <div class="py-10">
      <header>
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 class="text-3xl font-bold leading-tight text-yellow-600">
            Discover Strategy
          </h1>
          {timeLeft !== '' && <h3 class="text-lg leading-6 font-medium text-gray-900">
            <span class="text-red-700">{timeLeft}</span> time left to finish CREATE stage for this project. Create as much as possible for now! Wojtek need to push the team during this time.
          </h3>}
        </div>
      </header>
      <section>
        {/* <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 "> */}
        <div class="grid-container bg-yellow-300">
          <div class="bg-yellow-300 challenge-how-might" >
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={1}
              questionId="challenge-how"
              title="THE CHALLENGE: HOW MIGHT WE… "
              placeholder="How might we..."
              hint="Frame the challenge in the “how might we…” format, which frames the context clearly, but leaves options for solutions open! For example: “HMW speed up the experience of a solo entrepreneur with less than 100k revenue wen asking for help online while requesting a loan?”"
              voteQuestion1="How much it is important to you?"
              voteQuestion2="How clearly is here context framed?"
            />
          </div>
          <div class="bg-yellow-300 challenge-why">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={2}
              questionId="challenge-why"
              title="WHY THIS CHALLENGE?"
              placeholder=""
              hint="How did the team come up with this challenge? Why is it important for the team?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-customer">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={3}
              questionId="challenge-customer"
              title="CUSTOMER SEGMENT"
              placeholder=""
              hint="Who do you want to create value for? Are there existing Personas? Try to be as concrete as possible. “Entrepreneurs with less than 100k revenue” is concrete enough, “SME’s” is not."
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-ideas">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={4}
              questionId="challenge-ideas"
              title="FIRST SOLUTION IDEAS"
              placeholder=""
              hint="First ideas on how to solve the challenge? Why did you decide on these solutions? What is the competition doing?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-current">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={5}
              questionId="challenge-current"
              title="CURRENT SITUATION"
              placeholder=""
              hint="What is the current customer journey? Any pains or problems?
              What are customer’s alternatives and why are they not happy?
              What are the assumptions here? Which facts have already been validated?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-assumptions">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={6}
              questionId="challenge-assumptions"
              title="SOLUTION ASSUMPTIONS"
              placeholder=""
              hint="What do you think customers want to see changed? What have you seen work before?
              What do you think would make them more satisfied?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-succes">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={7}
              questionId="challenge-succes"
              title="GOAL/SUCCES"
              placeholder=""
              hint="What kind of decision do you want to make at the end of the project? What do you want to achieve? What do you expect the team to do and find out? What would be the successful outcome for you?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
          <div class="bg-yellow-300 challenge-related">
            <PostItFrame
              postIts={postItItems}
              project={project}
              user={user}
              questionNo={8}
              questionId="challenge-related"
              title="RELATED INITIATIVES & RESOURCES"
              placeholder=""
              hint="Are there any projects which recently explored similar business areas/challenges? Do you have any market research/reports done which could help us out? Are there any people we should speak to that might have more information about the challenge or the customer segment of the current solution?"
              voteQuestion1=""
              voteQuestion2=""
            />
          </div>
        </div>
        {/* </div> */}
      </section>
    </div>
  );
}

export default ScopingCanvas;

