import React,{ useState } from 'react';
import { v4 as uuid } from 'uuid';
import * as FirestoreService from '../services/firestore';


function PostItFrame({ project,postIts,questionId,questionNo,title,placholder,hint,voteQuestion1,voteQuestion2,user }) {
    const [newItem,setNewItem] = useState('');
    const [newComment,setNewComment] = useState('');
    const [isFocus,setIsFocus] = useState(false);
    const items = postIts.filter(postIt => postIt.questionId === questionId)
    const addItem = () => {
        if(newItem === '') {
            return;
        }
        const itemToAdd = {
            questionId,
            id: uuid(),
            text: newItem,
            createdBy: user,
            voteQuestion1,
            voteQuestion2,
            ...(voteQuestion1 && { vote1: {} }),
            ...(voteQuestion2 && { vote2: {} }),
        };

        FirestoreService.addPostItItem(project,itemToAdd);
        setNewItem('');
    };
    const addComment = (item) => () => {
        if(newComment === '') {
            return;
        }
        const itemToUpdate = {
            ...item,
            comments: [
                ...(item.comments ? item.comments : []),
                {
                    createdBy: user,
                    created: new Date,
                    text: newComment,
                }
            ]
        };

        FirestoreService.addPostItItem(project,itemToUpdate);

        setNewComment('');
    }
    return (
        <div class="w-full flex items-center justify-between p-3 border-b-2" title={hint}>
            <div class="flex-1">
                <div class={`mb-4 flex items-center space-x-3 ${isFocus && "flex-col"}`}>
                    <span class="flex-shrink-0 inline-block px-2 py-0.5 text-indigo-800 text-xs leading-4 font-medium bg-indigo-100 rounded-full">{questionNo}</span>
                    <h2 class="text-gray-900 text-sm leading-5 font-medium">{title}</h2>
                    {/* <label for="email" class="hidden text-sm font-medium leading-5 text-gray-700">Email</label> */}
                    <div class="w-full relative rounded-md shadow-sm">
                        <textarea
                            autocomplete="off" id="how-might-we"
                            value={newItem}
                            onChange={({ target }) => setNewItem(target.value)}
                            onFocus={() => setIsFocus(true)}
                            onBlur={() => setIsFocus(false)}
                            style={isFocus ? { height: 300 } : {}}
                            class="form-input block w-full sm:text-sm sm:leading-5" placeholder={placholder} ></textarea>
                    </div>
                    <span class="inline-flex rounded-md shadow-sm">
                        <button onClick={addItem} type="button" class="inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150">
                            Add
                        </button>
                    </span>
                </div>
                <p class="text-xs mb-2">{hint}</p>
                <ul class={`grid grid-cols-1 gap-3 sm:grid-cols-1 lg:grid-cols-3 items-start`}>
                    {items.map(item => (
                        <li class="col-span-1 bg-white rounded-lg shadow">
                            <div class="w-full flex items-start justify-between p-2 flex-col">
                                <div class="flex-1 ">
                                    <p class="mb-2 text-gray-900 text-m leading-5 ">{item.text}</p>
                                </div>
                            </div>
                            {item.comments && item.comments.map(comment => (
                                <div class="border-t w-full flex items-start justify-between p-2 flex-col">
                                    <div class="flex-1 ">
                                        <p class=" ml-2 text-gray-700 text-xs leading-2 "><b>{comment.createdBy}:</b> {comment.text}</p>
                                    </div>
                                </div>
                            ))}
                            <span class="inline-flex rounded-md shadow-sm w-full">
                                <div class="w-full mr-1 relative rounded-md shadow-sm">
                                    <input value={newComment} onChange={({ target }) => setNewComment(target.value)} class="form-input block w-full text-sm " placeholder="For feedback use: I like that... & I wish that..." />
                                </div>
                                <button onClick={addComment(item)} type="button" class="inline-flex items-center px-2.5 py-1.5 border border-gray-300 text-xs leading-4 font-medium rounded text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150">
                                    Comment
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        </div>

    );
}

export default PostItFrame;
